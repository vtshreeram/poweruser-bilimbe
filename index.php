<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Bilimbe Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- DataTables -->
  <link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css" />

</head>


<body>

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <div class="header-bg">
        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo-->
                    <div class="d-block d-lg-none mr-5">

                        <a href="index.php" class="logo">
                            <img src="assets/images/logo-sm.png" alt="" height="28" class="logo-small">
                        </a>

                    </div>
                    <!-- End Logo-->

                    <div class="menu-extras topbar-custom navbar p-0">

                        <ul class="list-inline flags-dropdown d-none d-lg-block mb-0">
                            <li class="list-inline-item text-white-50 mr-3">
                                <span class="font-13">Help : +012 3456 789</span>
                            </li>
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle text-white-50 arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/flags/us_flag.jpg" alt="" class="flag-img"> United States <i class="mdi mdi-chevron-down"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-animated">
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/french_flag.jpg" alt="" class="flag-img"> French</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/germany_flag.jpg" alt="" class="flag-img"> Germany</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/italy_flag.jpg" alt="" class="flag-img"> Italy</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/spain_flag.jpg" alt="" class="flag-img"> Spain</a>
                                </div>
                            </li>
                        </ul>

                        <!-- Search input -->
                        <div class="search-wrap" id="search-wrap">
                            <div class="search-bar">
                                <input class="search-input" type="search" placeholder="Search" />
                                <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                                    <i class="mdi mdi-close-circle"></i>
                                </a>
                            </div>
                        </div>

                        <ul class="list-inline ml-auto mb-0">

                            <!-- notification-->

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link waves-effect toggle-search" href="#" data-target="#search-wrap">
                                    <i class="mdi mdi-magnify noti-icon"></i>
                                </a>
                            </li>

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-bell-outline noti-icon"></i>
                                    <span class="badge badge-info badge-pill noti-icon-badge">3</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Notification (3)</h5>
                                    </div>

                                    <div class="slimscroll-noti">
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                            <p class="notify-details"><b>Your order is placed</b><span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-danger"><i class="mdi mdi-message-text-outline"></i></div>
                                            <p class="notify-details"><b>New Message received</b><span class="text-muted">You have 87 unread messages</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-info"><i class="mdi mdi-filter-outline"></i></div>
                                            <p class="notify-details"><b>Your item is shipped</b><span class="text-muted">It is a long established fact that a reader will</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-message-text-outline"></i></div>
                                            <p class="notify-details"><b>New Message received</b><span class="text-muted">You have 87 unread messages</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-warning"><i class="mdi mdi-cart-outline"></i></div>
                                            <p class="notify-details"><b>Your order is placed</b><span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                        </a>

                                    </div>


                                    <!-- All-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-all">
                                            View All
                                        </a>

                                </div>
                            </li>
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-6.jpg" alt="user" class="rounded-circle">
                                    <span class="d-none d-md-inline-block ml-1">Donald T. <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                                    <a class="dropdown-item" href="profile.php"><i class="dripicons-user text-muted"></i> Profile</a>
                                    <a class="dropdown-item" href="payment.php"><i class="dripicons-wallet text-muted"></i> My Wallet</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"><i class="dripicons-exit text-muted"></i> Logout</a>
                                </div>
                            </li>
                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>

                        </ul>

                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div>
                <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <!-- Logo-->
                    <div class="d-none d-lg-block">
                        <!-- Text Logo
                            <a href="index.html" class="logo">
                                Foxia
                            </a>
                             -->
                        <!-- Image Logo -->
                        <a href="index.php" class="logo">
                            <!-- <img src="assets/images/logo-sm.png" alt="" height="22" class="logo-small"> -->
                            <img src="assets/images/logo.png" alt="" height="20" class="logo-large">
                        </a>

                    </div>
                    <!-- End Logo-->
                    <div id="navigation">

                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu">
                                <a href="index.php"><i class="dripicons-meter"></i>Dashboard</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-users.php"><i class="dripicons-user"></i>Users</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-photos-grid.php"><i class="dripicons-photo-group"></i>Photos</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-api-connections.php"><i class="dripicons-document"></i>Api</a>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mb-0">Dashboard</h4>
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="/">Bilimbe</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </div>
                            <div class="col-md-4">
                                <div class="float-right d-none d-md-block">
                                    <div class="dropdown">
                                        <button class="btn btn-light btn-rounded dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="ti-settings mr-1"></i> Settings
                                            </button>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Separated link</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->

        </div>
    </div>


    <div class="wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">
                                <div class="float-right text-right">
                                    <span class="badge badge-light text-info mt-2 mb-2"> + 20% </span>
                                    <p class="text-white-50">From previous period</p>
                                </div>

                                <span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}' data-width="54" data-height="54">5/8</span>
                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">1345</h5>
                                <h6 class="mt-0 mb-3">No. of users</h6>
                                <p class="text-muted mb-0">Total number of users</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">
                                <div class="float-right text-right">
                                    <span class="badge badge-light text-info mt-2 mb-2"> + 20% </span>
                                    <p class="text-white-50">From previous period</p>
                                </div>

                                <span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}' data-width="54" data-height="54">5/8</span>
                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">2000</h5>
                                <h6 class="mt-0 mb-3">Total no. of photos</h6>
                                <p class="text-muted mb-0">Total number of photos</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">
                                <div class="float-right text-right">
                                    <span class="badge badge-light text-info mt-2 mb-2"> + 20% </span>
                                    <p class="text-white-50">From previous period</p>
                                </div>

                                <span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}' data-width="54" data-height="54">5/8</span>
                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">1345</h5>
                                <h6 class="mt-0 mb-3">Total shares</h6>
                                <p class="text-muted mb-0">Total number of shares</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">
                                <div class="float-right text-right">
                                    <span class="badge badge-light text-info mt-2 mb-2"> + 20% </span>
                                    <p class="text-white-50">From previous period</p>
                                </div>

                                <span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}' data-width="54" data-height="54">5/8</span>
                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">1345</h5>
                                <h6 class="mt-0 mb-3">Whatsapp shares</h6>
                                <p class="text-muted mb-0">Count of Whatsapp shares</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">
                                <div class="float-right text-right">
                                    <span class="badge badge-light text-info mt-2 mb-2"> + 20% </span>
                                    <p class="text-white-50">From previous period</p>
                                </div>

                                <span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}' data-width="54" data-height="54">5/8</span>
                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">1345</h5>
                                <h6 class="mt-0 mb-3">Email shares</h6>
                                <p class="text-muted mb-0">Count of email shares</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">
                                <div class="float-right text-right">
                                    <span class="badge badge-light text-info mt-2 mb-2"> + 20% </span>
                                    <p class="text-white-50">From previous period</p>
                                </div>

                                <span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}' data-width="54" data-height="54">5/8</span>
                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">1345</h5>
                                <h6 class="mt-0 mb-3">Instagram shares</h6>
                                <p class="text-muted mb-0">Count of Instagram shares</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card mini-stats">
                        <div class="p-3 mini-stats-content">
                            <div class="mb-4">
                                <div class="float-right text-right">
                                    <span class="badge badge-light text-info mt-2 mb-2"> + 20% </span>
                                    <p class="text-white-50">From previous period</p>
                                </div>

                                <span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}' data-width="54" data-height="54">5/8</span>
                            </div>
                        </div>
                        <div class="ml-3 mr-3">
                            <div class="bg-white p-3 mini-stats-desc rounded">
                                <h5 class="float-right mt-0">1345</h5>
                                <h6 class="mt-0 mb-3">FB page connected</h6>
                                <p class="text-muted mb-0">Total FB page connected</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

         

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Latest Transactions</h4>
                            <div class="table-responsive mt-4">
                            <table id="datatable" class="userTable table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">No. Photos</th>
                                            <th scope="col">No. Shared</th>
                                            <th scope="col">No. Email</th>
                                            <th scope="col">No. FB</th>
                                            <th scope="col">No. Insta</th>
                                            <th scope="col">No. Whatsapp</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">HP123</th>
                                            <td>
                                                12/10/2020
                                            </td>
                                            <td>Sam</td>
                                            <td>sam@gmail.com</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                            <td>123456</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end wrapper -->


    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2020 Bilimbe 
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>

    <script src="plugins/peity-chart/jquery.peity.min.js"></script>

    <!--Morris Chart-->
    <script src="plugins/morris/morris.min.js"></script>
    <script src="plugins/raphael/raphael-min.js"></script>

    <script src="assets/pages/dashboard.js"></script>

     <!-- Required datatable js -->
     <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="plugins/datatables/jszip.min.js"></script>
    <script src="plugins/datatables/pdfmake.min.js"></script>
    <script src="plugins/datatables/vfs_fonts.js"></script>
    <script src="plugins/datatables/buttons.html5.min.js"></script>
    <script src="plugins/datatables/buttons.print.min.js"></script>
    <script src="plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Datatable init js -->
    <script src="assets/pages/datatables.init.js"></script>
    <!-- App js -->
    <script src="assets/js/app.js"></script>

</body>

</html>