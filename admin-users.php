<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Bilimbe Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- DataTables -->
    <link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css" />

</head>


<body>

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <div class="header-bg">
        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo-->
                    <div class="d-block d-lg-none mr-5">

                        <a href="index.php" class="logo">
                            <img src="assets/images/logo-sm.png" alt="" height="28" class="logo-small">
                        </a>

                    </div>
                    <!-- End Logo-->

                    <div class="menu-extras topbar-custom navbar p-0">

                        <ul class="list-inline flags-dropdown d-none d-lg-block mb-0">
                            <li class="list-inline-item text-white-50 mr-3">
                                <span class="font-13">Help : +012 3456 789</span>
                            </li>
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle text-white-50 arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/flags/us_flag.jpg" alt="" class="flag-img"> United States <i class="mdi mdi-chevron-down"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-animated">
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/french_flag.jpg" alt="" class="flag-img"> French</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/germany_flag.jpg" alt="" class="flag-img"> Germany</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/italy_flag.jpg" alt="" class="flag-img"> Italy</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/spain_flag.jpg" alt="" class="flag-img"> Spain</a>
                                </div>
                            </li>
                        </ul>

                        <!-- Search input -->
                        <div class="search-wrap" id="search-wrap">
                            <div class="search-bar">
                                <input class="search-input" type="search" placeholder="Search" />
                                <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                                    <i class="mdi mdi-close-circle"></i>
                                </a>
                            </div>
                        </div>

                        <ul class="list-inline ml-auto mb-0">

                            <!-- notification-->

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link waves-effect toggle-search" href="#" data-target="#search-wrap">
                                    <i class="mdi mdi-magnify noti-icon"></i>
                                </a>
                            </li>

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-bell-outline noti-icon"></i>
                                    <span class="badge badge-info badge-pill noti-icon-badge">3</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Notification (3)</h5>
                                    </div>

                                    <div class="slimscroll-noti">
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                            <p class="notify-details"><b>Your order is placed</b><span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-danger"><i class="mdi mdi-message-text-outline"></i></div>
                                            <p class="notify-details"><b>New Message received</b><span class="text-muted">You have 87 unread messages</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-info"><i class="mdi mdi-filter-outline"></i></div>
                                            <p class="notify-details"><b>Your item is shipped</b><span class="text-muted">It is a long established fact that a reader will</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-message-text-outline"></i></div>
                                            <p class="notify-details"><b>New Message received</b><span class="text-muted">You have 87 unread messages</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-warning"><i class="mdi mdi-cart-outline"></i></div>
                                            <p class="notify-details"><b>Your order is placed</b><span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                        </a>

                                    </div>


                                    <!-- All-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-all">
                                            View All
                                        </a>

                                </div>
                            </li>
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-6.jpg" alt="user" class="rounded-circle">
                                    <span class="d-none d-md-inline-block ml-1">Donald T. <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                                <a class="dropdown-item" href="profile.php"><i class="dripicons-user text-muted"></i> Profile</a>
                                    <a class="dropdown-item" href="payment.php"><i class="dripicons-wallet text-muted"></i> My Wallet</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"><i class="dripicons-exit text-muted"></i> Logout</a>
                                </div>
                            </li>
                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>

                        </ul>

                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div>
                <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <!-- Logo-->
                    <div class="d-none d-lg-block">
                        <!-- Text Logo
                            <a href="index.html" class="logo">
                                Foxia
                            </a>
                             -->
                        <!-- Image Logo -->
                        <a href="index.php" class="logo">
                            <!-- <img src="assets/images/logo-sm.png" alt="" height="22" class="logo-small"> -->
                            <img src="assets/images/logo.png" alt="" height="20" class="logo-large">
                        </a>

                    </div>
                    <!-- End Logo-->
                    <div id="navigation">

                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                        <li class="has-submenu">
                                <a href="index.php"><i class="dripicons-meter"></i>Dashboard</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-users.php"><i class="dripicons-user"></i>Users</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-photos-grid.php"><i class="dripicons-photo-group"></i>Photos</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-api-connections.php"><i class="dripicons-document"></i>Api</a>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mb-0">Users</h4>
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="#">HP</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                                </ol>
                            </div>
                            <div class="col-md-4">
                                <div class="float-right d-none d-md-block">
                                <button type="button" class="btn btn-light btn-lg waves-effect waves-light">Create New</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->

        </div>
    </div>


    <div class="wrapper">
        <div class="container-fluid full-width">

        <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="datatable" class="userTable table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>SNo.</th>
                                        <th>Username</th>
                                        <th>Password</th>
                                        <th>Email Address</th>
                                        <th>Modules</th>
                                        <th>Logs</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Sam</td>
                                        <td>sam123</td>
                                        <td>sam@gmail.com</td>
                                        <td>
                                        <div class="checkbox-wrap d-flex">
                                                    <label class="custom-check">
                                                        <input type="checkbox" checked="">
                                                        <span>SIM</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GIF</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>col</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>Mul</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GRE</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GRV</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>ARM</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>COF</span>
                                                    </label>
                                                </div>
                                        </td>
                                        <td>
                                            <a href="#"><i class="dripicons-blog"></i></a>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-dark waves-effect waves-light">Edit</button>
                                            <button type="button" class="btn btn-danger waves-effect waves-light">Delete</button>
                                            <button type="button" class="btn btn-success waves-effect waves-light">Update</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Sam</td>
                                        <td>sam123</td>
                                        <td>sam@gmail.com</td>
                                        <td>
                                        <div class="checkbox-wrap d-flex">
                                                    <label class="custom-check">
                                                        <input type="checkbox" checked="">
                                                        <span>SIM</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GIF</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>col</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>Mul</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GRE</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GRV</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>ARM</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>COF</span>
                                                    </label>
                                                </div>
                                        </td>
                                        <td>
                                            <a href="#"><i class="dripicons-blog"></i></a>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-dark waves-effect waves-light">Edit</button>
                                            <button type="button" class="btn btn-danger waves-effect waves-light">Delete</button>
                                            <button type="button" class="btn btn-success waves-effect waves-light">Update</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Sam</td>
                                        <td>sam123</td>
                                        <td>sam@gmail.com</td>
                                        <td>
                                        <div class="checkbox-wrap d-flex">
                                                    <label class="custom-check">
                                                        <input type="checkbox" checked="">
                                                        <span>SIM</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GIF</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>col</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>Mul</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GRE</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>GRV</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>ARM</span>
                                                    </label>
                                                    <label class="custom-check">
                                                        <input type="checkbox">
                                                        <span>COF</span>
                                                    </label>
                                                </div>
                                        </td>
                                        <td>
                                            <a href="#"><i class="dripicons-blog"></i></a>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-dark waves-effect waves-light">Edit</button>
                                            <button type="button" class="btn btn-danger waves-effect waves-light">Delete</button>
                                            <button type="button" class="btn btn-success waves-effect waves-light">Update</button>
                                        </td>
                                    </tr>
                                    
                                    
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end wrapper -->


    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2020 Bilimbe 
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>

    <!-- Required datatable js -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="plugins/datatables/jszip.min.js"></script>
    <script src="plugins/datatables/pdfmake.min.js"></script>
    <script src="plugins/datatables/vfs_fonts.js"></script>
    <script src="plugins/datatables/buttons.html5.min.js"></script>
    <script src="plugins/datatables/buttons.print.min.js"></script>
    <script src="plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Datatable init js -->
    <script src="assets/pages/datatables.init.js"></script>

    <!-- App js -->
    <script src="assets/js/app.js"></script>

</body>

</html>