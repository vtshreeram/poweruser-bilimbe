<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Bilimbe Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Magnific popup -->
        <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css" />

</head>


<body>

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <div class="header-bg">
        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo-->
                    <div class="d-block d-lg-none mr-5">

                        <a href="index.php" class="logo">
                            <img src="assets/images/logo-sm.png" alt="" height="28" class="logo-small">
                        </a>

                    </div>
                    <!-- End Logo-->

                    <div class="menu-extras topbar-custom navbar p-0">

                        <ul class="list-inline flags-dropdown d-none d-lg-block mb-0">
                            <li class="list-inline-item text-white-50 mr-3">
                                <span class="font-13">Help : +012 3456 789</span>
                            </li>
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle text-white-50 arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/flags/us_flag.jpg" alt="" class="flag-img"> United States <i class="mdi mdi-chevron-down"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-animated">
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/french_flag.jpg" alt="" class="flag-img"> French</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/germany_flag.jpg" alt="" class="flag-img"> Germany</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/italy_flag.jpg" alt="" class="flag-img"> Italy</a>
                                    <a href="#" class="dropdown-item"><img src="assets/images/flags/spain_flag.jpg" alt="" class="flag-img"> Spain</a>
                                </div>
                            </li>
                        </ul>

                        <!-- Search input -->
                        <div class="search-wrap" id="search-wrap">
                            <div class="search-bar">
                                <input class="search-input" type="search" placeholder="Search" />
                                <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                                    <i class="mdi mdi-close-circle"></i>
                                </a>
                            </div>
                        </div>

                        <ul class="list-inline ml-auto mb-0">

                            <!-- notification-->

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link waves-effect toggle-search" href="#" data-target="#search-wrap">
                                    <i class="mdi mdi-magnify noti-icon"></i>
                                </a>
                            </li>

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-bell-outline noti-icon"></i>
                                    <span class="badge badge-info badge-pill noti-icon-badge">3</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Notification (3)</h5>
                                    </div>

                                    <div class="slimscroll-noti">
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                            <p class="notify-details"><b>Your order is placed</b><span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-danger"><i class="mdi mdi-message-text-outline"></i></div>
                                            <p class="notify-details"><b>New Message received</b><span class="text-muted">You have 87 unread messages</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-info"><i class="mdi mdi-filter-outline"></i></div>
                                            <p class="notify-details"><b>Your item is shipped</b><span class="text-muted">It is a long established fact that a reader will</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-message-text-outline"></i></div>
                                            <p class="notify-details"><b>New Message received</b><span class="text-muted">You have 87 unread messages</span></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-warning"><i class="mdi mdi-cart-outline"></i></div>
                                            <p class="notify-details"><b>Your order is placed</b><span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                        </a>

                                    </div>


                                    <!-- All-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-all">
                                            View All
                                        </a>

                                </div>
                            </li>
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-6.jpg" alt="user" class="rounded-circle">
                                    <span class="d-none d-md-inline-block ml-1">Donald T. <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                                <a class="dropdown-item" href="profile.php"><i class="dripicons-user text-muted"></i> Profile</a>
                                    <a class="dropdown-item" href="payment.php"><i class="dripicons-wallet text-muted"></i> My Wallet</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"><i class="dripicons-exit text-muted"></i> Logout</a>
                                </div>
                            </li>
                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>

                        </ul>

                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div>
                <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <!-- Logo-->
                    <div class="d-none d-lg-block">
                        <!-- Text Logo
                            <a href="index.html" class="logo">
                                Foxia
                            </a>
                             -->
                        <!-- Image Logo -->
                        <a href="index.php" class="logo">
                            <!-- <img src="assets/images/logo-sm.png" alt="" height="22" class="logo-small"> -->
                            <img src="assets/images/logo.png" alt="" height="20" class="logo-large">
                        </a>

                    </div>
                    <!-- End Logo-->
                    <div id="navigation">

                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                        <li class="has-submenu">
                                <a href="index.php"><i class="dripicons-meter"></i>Dashboard</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-users.php"><i class="dripicons-user"></i>Users</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-photos-grid.php"><i class="dripicons-photo-group"></i>Photos</a>
                            </li>
                            <li class="has-submenu">
                                <a href="admin-api-connections.php"><i class="dripicons-document"></i>Api</a>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mb-0">Photos</h4>
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="#">Loren Ipsum</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Loren Ipsum</li>
                                </ol>
                            </div>
                            <div class="col-md-4">
                                <div class="float-right d-none d-md-block">
                                    <div class="dropdown">
                                        <button class="btn btn-light btn-rounded dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="ti-settings mr-1"></i> Settings
                                            </button>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Separated link</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->

        </div>
    </div>


    <div class="wrapper">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Table cta -->
                        <div class="table-cta d-flex align-items-center">
                            <div class="item">
                                <select class="form-control">
                                    <option value="" style="display: none;">Select User</option>
                                    <option value="">User 1</option>
                                    <option value="">User 2</option>
                                </select>
                            </div>
                            <div class="item d-flex">
                                <label class="label">
                                    Date From
                                </label>
                            <input class="form-control" type="date" value="" id="example-date-input">
                            </div>
                            <div class="item d-flex">
                            <label class="label">
                                    Date To
                                </label>
                            <input class="form-control" type="date" value="" id="example-date-input2">
                            </div>
                            <div class="item">
                                <div class="radio-wrap">
                                    <label class="custom-radio">
                                        <input type="radio" name="downloadType" checked >
                                        <span>Email</span>
                                    </label>
                                    <label class="custom-radio">
                                        <input type="radio" name="downloadType">
                                        <span>Photos</span>
                                    </label>
                                </div>
                            </div>
                            <div class="item">
                            <button type="button" class="btn btn-success waves-effect waves-light">Download Data</button>
                            <button type="button" class="btn btn-success waves-effect waves-light">Download Photos</button>
                            </div>
                        </div>
                        <!-- ./Table cta -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                <h4 class="mt-0 header-title">Username</h4>
                                </div>
                                <div class="col-6 text-right grid-list">
                                    <a href="admin-photos-grid.php" class="active">Grid | </a>
                                    <a href="admin-photos-list.php"> List</a>
                                </div>
                            </div>
                            <br>
                            <div class="image-grid">
                            <div class="popup-gallery2">
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                    <a class="lightItem" href="assets/images/small/img-1.jpg" title="Project 1">
                                        <div class="img-fluid d-block">
                                            <img src="assets/images/small/img-1.jpg" alt="" width="120">
                                        </div>
                                    </a>
                                </div>
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-end mb-0">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#">Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end wrapper -->


    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2020 Bilimbe 
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
        <!-- Magnific popup -->
        <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="assets/pages/lightbox.js"></script>

    <!-- App js -->
    <script src="assets/js/app.js"></script>
    <script>
        $('.popup-gallery2').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
		}
	});
    </script>

</body>

</html>